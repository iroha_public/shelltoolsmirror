#日本語 (こちらを優先します。)
このスクリプト群はIroha Aoyagi (青柳 彩葉,以下、私)によって書かれました。
すべての権利は私にあります。
! いかなる理由においても、二次利用は禁止です。 !
また、サポートは一切いたしかねますので、慎重な判断の上で、自己責任で使用してください。
対応は一切しません。

#EN by Deepl (Priority is given to Japanese content!)
This set of scripts was written by Iroha Aoyagi (青柳 彩葉).
! All rights reserved. Secondary use for any reason is prohibited. !
Please use at your own risk and use with caution.
I will not respond to any correspondence.
