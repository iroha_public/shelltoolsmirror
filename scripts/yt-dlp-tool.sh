#!/usr/bin/env bash

set -eu

if [ "$1" = "" ]; then
    echo -e "不正なオプションが検出されました！\\nヘルプ(-h)を実行してください。"
    exit 1
fi

# ヘルプ
if [ "$1" = -h ]; then
    echo -e "##############################ヘルプ##############################\\nダウンロード先を指定したい時は、-oを最初に入力してください。\\n例:-o /home/Username/Downloads URL1 ...\\n指定しない場合は入力しないでください。\\n不正な入力がされた場合、深刻なエラーが発生します！\\n禁止される例:-o URL1 ...\\n\\n存在しないディレクトリは指定できません！\\nパーミッションもよく確認してください！\\n処理を中断することはおすすめしません！\\n\\nデフォルトでは、カレントディレクトリに保存します。\\n\\n\\nURLは続けて、複数入力できます。\\n#################################################################"
    exit 0
fi

# 処理
if [ "$1" = -o ]; then
    if [ -d "$2" ]; then
        cd "$2"
        echo -e "Using $(pwd)"
        for _i in $(seq 3 ${#})
        do
            yt-dlp -f "bv[ext=webm]+ba[ext=webm]/b" "${3}"
            shift
        done
        else
        echo "存在しないディレクトリが指定されました！"
        exit 1
    fi
else
echo -e "Using $(pwd)"
for _i in $(seq 1 ${#})
do
    yt-dlp -f "bv[ext=webm]+ba[ext=webm]/b" "${1}"
    shift
done
fi

exit 0

# EOF
